#ifndef I2P2_VECTOR_H
#define I2P2_VECTOR_H

#include "I2P2_container.h"
#include "I2P2_iterator.h"
#include <iostream>

namespace I2P2 {
class Vector final : public randomaccess_container {
  private:
    pointer _begin   = nullptr;
    pointer _end     = nullptr;
    pointer _end_cap = nullptr;

    static size_t _next_id;
    size_t _id;

    // you may want to declare the necessary members for your Vector here

    /* The following are standard methods from the STL
     * If you are not sure what they do
     * look them up and implement your own version */
  public:
    ~Vector();
    Vector();
    Vector(const Vector& rhs);
    Vector& operator=(const Vector& rhs);
    virtual iterator begin() override;
    virtual const_iterator begin() const override;
    virtual iterator end() override;
    virtual const_iterator end() const override;
    virtual reference front() override;
    virtual const_reference front() const override;
    virtual reference back() override;
    virtual const_reference back() const override;
    virtual reference operator[](size_type pos) override;
    virtual const_reference operator[](size_type pos) const override;
    virtual size_type capacity() const override;
    virtual size_type size() const override;
    virtual void clear() override;
    virtual bool empty() const override;
    virtual void erase(const_iterator pos) override;
    virtual void erase(const_iterator begin, const_iterator end) override;
    virtual void
    insert(const_iterator pos, size_type count, const_reference val) override;
    virtual void insert(
        const_iterator pos, const_iterator begin, const_iterator end) override;
    virtual void pop_back() override;
    virtual void pop_front() override;
    virtual void push_back(const_reference val) override;
    virtual void push_front(const_reference val) override;
    virtual void reserve(size_type new_capacity) override;
    virtual void shrink_to_fit() override;

    void dump() const;
};
} // namespace I2P2

#endif

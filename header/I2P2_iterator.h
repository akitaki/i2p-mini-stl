#ifndef I2P2_ITERATOR_H
#define I2P2_ITERATOR_H
#include "I2P2_def.h"
#include <iterator>

namespace I2P2 {
struct Node {
    Node* left      = nullptr;
    Node* right     = nullptr;
    value_type data = 0;
    bool is_dummy   = false;
    inline Node() : is_dummy(true) {}
    inline Node(value_type data, Node* left) : data(data), left(left) {}
};

struct iterator_impl_base {
    virtual reference operator*() const                                    = 0;
    virtual reference operator[](difference_type offset) const             = 0;
    virtual pointer operator->() const                                     = 0;
    virtual difference_type operator-(const iterator_impl_base& rhs) const = 0;
    virtual iterator_impl_base& operator++()                               = 0;
    virtual iterator_impl_base& operator--()                               = 0;
    virtual iterator_impl_base& operator+=(difference_type offset)         = 0;
    virtual iterator_impl_base& operator-=(difference_type offset)         = 0;
    virtual bool operator==(const iterator_impl_base& rhs) const           = 0;
    virtual bool operator!=(const iterator_impl_base& rhs) const           = 0;
    virtual bool operator<(const iterator_impl_base& rhs) const            = 0;
    virtual bool operator>(const iterator_impl_base& rhs) const            = 0;
    virtual bool operator<=(const iterator_impl_base& rhs) const           = 0;
    virtual bool operator>=(const iterator_impl_base& rhs) const           = 0;
    inline virtual ~iterator_impl_base() {}

    virtual iterator_impl_base* clone() const = 0;
    /* This is the base class of all the container-specialized iterators
     * In order to invoke a derived function from this class
     * you may have to either do a downcast or invoke from a virtual function */
};

class vector_iterator : public iterator_impl_base {
  protected:
    pointer _ptr;

    friend class Vector;

  public:
    vector_iterator();
    vector_iterator(pointer ptr) : _ptr(ptr) {}
    virtual iterator_impl_base& operator++() override;
    virtual iterator_impl_base& operator--() override;
    virtual iterator_impl_base& operator+=(difference_type offset) override;
    virtual iterator_impl_base& operator-=(difference_type offset) override;
    virtual bool operator==(const iterator_impl_base& rhs) const override;
    virtual bool operator!=(const iterator_impl_base& rhs) const override;
    virtual bool operator<(const iterator_impl_base& rhs) const override;
    virtual bool operator>(const iterator_impl_base& rhs) const override;
    virtual bool operator<=(const iterator_impl_base& rhs) const override;
    virtual bool operator>=(const iterator_impl_base& rhs) const override;
    virtual difference_type
    operator-(const iterator_impl_base& rhs) const override;
    virtual pointer operator->() const override;
    virtual reference operator*() const override;
    virtual reference operator[](difference_type offset) const override;

    inline virtual iterator_impl_base* clone() const override {
        auto it  = new vector_iterator();
        it->_ptr = this->_ptr;
        return it;
    }
};

class list_iterator : public iterator_impl_base {
  protected:
    Node* _node;

  public:
    list_iterator();
    list_iterator(Node* node) : _node(node) {}
    virtual iterator_impl_base& operator++() override;
    virtual iterator_impl_base& operator--() override;
    virtual iterator_impl_base& operator+=(difference_type offset) override;
    virtual iterator_impl_base& operator-=(difference_type offset) override;
    virtual bool operator==(const iterator_impl_base& rhs) const override;
    virtual bool operator!=(const iterator_impl_base& rhs) const override;
    virtual bool operator<(const iterator_impl_base& rhs) const override;
    virtual bool operator>(const iterator_impl_base& rhs) const override;
    virtual bool operator<=(const iterator_impl_base& rhs) const override;
    virtual bool operator>=(const iterator_impl_base& rhs) const override;
    virtual difference_type
    operator-(const iterator_impl_base& rhs) const override;
    virtual pointer operator->() const override;
    virtual reference operator*() const override;
    virtual reference operator[](difference_type offset) const override;

    inline Node* get_node() { return _node; }

    inline virtual iterator_impl_base* clone() const override {
        auto it = new list_iterator();
        it->_node = _node;
        return it;
    }
};

class const_iterator {
  public:
    using difference_type   = I2P2::difference_type;
    using value_type        = I2P2::value_type;
    using pointer           = I2P2::const_pointer;
    using reference         = I2P2::const_reference;
    using iterator_category = std::random_access_iterator_tag;

  protected:
    iterator_impl_base* _p = nullptr;

  public:
    ~const_iterator();
    const_iterator();
    const_iterator(const const_iterator& rhs);
    const_iterator(iterator_impl_base* p);
    const_iterator& operator=(const const_iterator& rhs);
    const_iterator& operator++();
    const_iterator operator++(int);
    const_iterator& operator--();
    const_iterator operator--(int);
    const_iterator& operator+=(difference_type offset);
    const_iterator operator+(difference_type offset) const;
    const_iterator& operator-=(difference_type offset);
    const_iterator operator-(difference_type offset) const;
    difference_type operator-(const const_iterator& rhs) const;
    pointer operator->() const;
    reference operator*() const;
    reference operator[](difference_type offset) const;
    bool operator==(const const_iterator& rhs) const;
    bool operator!=(const const_iterator& rhs) const;
    bool operator<(const const_iterator& rhs) const;
    bool operator>(const const_iterator& rhs) const;
    bool operator<=(const const_iterator& rhs) const;
    bool operator>=(const const_iterator& rhs) const;

    inline iterator_impl_base* raw() { return this->_p; }
    /* This class holds an iterator_impl_base
     * and you may want to have some ways to
     * invoke a container-specialized method from here
     * for insert/erase methods (look at their parameters if you are not sure)
     */
};

class iterator : public const_iterator {
  public:
    using difference_type   = I2P2::difference_type;
    using value_type        = I2P2::value_type;
    using pointer           = I2P2::pointer;
    using reference         = I2P2::reference;
    using iterator_category = std::random_access_iterator_tag;

  public:
    iterator();
    iterator(iterator_impl_base* p);
    iterator(const iterator& rhs);
    iterator& operator++();
    iterator operator++(int);
    iterator& operator--();
    iterator operator--(int);
    iterator& operator+=(difference_type offset);
    iterator operator+(difference_type offset) const;
    iterator& operator-=(difference_type offset);
    iterator operator-(difference_type offset) const;
    difference_type operator-(const iterator& rhs) const;
    pointer operator->() const;
    reference operator*() const;
    reference operator[](difference_type offset) const;
};
} // namespace I2P2

#endif

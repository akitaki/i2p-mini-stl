#include "../header/I2P2_iterator.h"
#include <iostream>

namespace I2P2 {

const_iterator::const_iterator() : _p(nullptr) {}

const_iterator::const_iterator(const const_iterator& rhs) : _p(nullptr) {
    this->_p = rhs._p->clone();
}

const_iterator::const_iterator(iterator_impl_base* p) : _p(nullptr) { this->_p = p->clone(); }

const_iterator::~const_iterator() {
    // Delete iter impl
    if (this->_p != nullptr) {
        delete this->_p;
    }
}

const_iterator& const_iterator::operator=(const const_iterator& rhs) {
    // Delete old iter impl
    if (this->_p != nullptr) {
        delete this->_p;
    }

    this->_p = rhs._p->clone();
    return *this;
}

const_iterator& const_iterator::operator++() {
    // Modify iter impl
    ++(*(this->_p));
    return *this;
}

const_iterator const_iterator::operator++(int) {
    // Clone `this` and modify
    const_iterator tmp{*this};
    ++(*(this->_p));
    return tmp;
}

const_iterator& const_iterator::operator--() {
    // Modify iter impl
    --(*(this->_p));
    return *this;
}

const_iterator const_iterator::operator--(int) {
    // Clone `this` and modify
    const_iterator tmp{*this};
    --(*(this->_p));
    return tmp;
}

const_iterator& const_iterator::operator+=(difference_type offset) {
    // Modify underlying iter impl
    *(this->_p) += offset;
    return *this;
}

const_iterator const_iterator::operator+(difference_type offset) const {
    // Copy `this` and offset it
    const_iterator tmp = *this;
    tmp._p->operator+=(offset);
    return tmp;
}

const_iterator& const_iterator::operator-=(difference_type offset) {
    // Modify underlying iter impl
    *(this->_p) -= offset;
    return *this;
}

const_iterator const_iterator::operator-(difference_type offset) const {
    // Copy `this` and offset it
    const_iterator tmp = *this;
    tmp._p->operator-=(offset);
    return tmp;
}

difference_type const_iterator::operator-(const const_iterator& rhs) const {
    // Delegate work to iter impl
    return *(this->_p) - *(rhs._p);
}

const_iterator::pointer const_iterator::operator->() const {
    // Delegate work to iter impl
    return this->_p->operator->();
}

const_iterator::reference const_iterator::operator*() const {
    // The first `*` dereferences `iterator_impl_base*`.
    // The second `*` calls `iterator_impl_base::operator*()`, which returns the
    // underlying reference.
    return *(*(this->_p));
}

const_iterator::reference
const_iterator::operator[](difference_type offset) const {
    // Delegate work to iter impl
    return (*(this->_p))[offset];
}

bool const_iterator::operator==(const const_iterator& rhs) const {
    // Delegate work to iter impl
    return *(this->_p) == *(rhs._p);
}

bool const_iterator::operator!=(const const_iterator& rhs) const {
    // Delegate work to iter impl
    return *(this->_p) != *(rhs._p);
}

bool const_iterator::operator<(const const_iterator& rhs) const {
    // Delegate work to iter impl
    return *(this->_p) < *(rhs._p);
}

bool const_iterator::operator>(const const_iterator& rhs) const {
    // Delegate work to iter impl
    return *(this->_p) > *(rhs._p);
}

bool const_iterator::operator<=(const const_iterator& rhs) const {
    // Delegate work to iter impl
    return *(this->_p) <= *(rhs._p);
}

bool const_iterator::operator>=(const const_iterator& rhs) const {
    // Delegate work to iter impl
    return *(this->_p) >= *(rhs._p);
}
} // namespace I2P2

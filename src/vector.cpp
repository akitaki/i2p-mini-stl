#include "../header/I2P2_Vector.h"
#include "../header/logger.h"
#include <algorithm>
#include <iostream>

namespace I2P2 {
size_t Vector::_next_id = 0;

/* Constructors and destructors */
Vector::Vector() : _id(_next_id++) {
    Logger() << "> ctor " << _id << "\n";
    this->_begin   = static_cast<pointer>(operator new(sizeof(value_type) * 0));
    this->_end     = _begin;
    this->_end_cap = _begin;
}

Vector::Vector(const Vector& rhs) : _id(_next_id++) {
    Logger() << "> copyctor " << _id << " " << rhs._id << "\n";
    this->_begin   = static_cast<pointer>(operator new(sizeof(value_type) * 0));
    this->_end     = _begin;
    this->_end_cap = _begin;

    this->reserve(rhs.size());

    // Place new elements
    for (size_t i = 0; i < rhs.size(); i++) {
        new (_begin + i) value_type(rhs[i]);
    }

    this->_end = _begin + rhs.size();
}

Vector& Vector::operator=(const Vector& rhs) {
    Logger() << "> asgnctor " << _id << " " << rhs._id << "\n";

    if (this == &rhs) {
        return *this;
    }

    // Destruct all old elements (cleanup)
    for (size_t i = 0; i < this->size(); i++) {
        _begin[i].~value_type();
    }

    this->_end = this->_begin;

    // Reserve space for new elements
    this->reserve(rhs.size());

    // Place new elements
    for (size_t i = 0; i < rhs.size(); i++) {
        new (_begin + i) value_type(rhs[i]);
    }

    this->_end = _begin + rhs.size();

    return *this;
}

Vector::~Vector() {
    Logger() << "> dtor " << _id << "\n";
    operator delete (this->_begin);
}

iterator Vector::begin() {
    vector_iterator impl(_begin);
    iterator it(&impl);
    return it;
}

const_iterator Vector::begin() const {
    vector_iterator impl(_begin);
    const_iterator it(&impl);
    return it;
}

iterator Vector::end() {
    vector_iterator impl(_end);
    iterator it(&impl);
    return it;
}

const_iterator Vector::end() const {
    vector_iterator impl(_end);
    const_iterator it(&impl);
    return it;
}

reference Vector::front() {
    return *_begin;
}

const_reference Vector::front() const {
    return *_begin;
}

reference Vector::back() {
    return *(_begin + size() - 1);
}

const_reference Vector::back() const {
    return *(_begin + size() - 1);
}

reference Vector::operator[](size_type pos) {
    return _begin[pos];
}

const_reference Vector::operator[](size_type pos) const {
    return _begin[pos];
}

size_type Vector::capacity() const {
    return _end_cap - _begin;
}

size_type Vector::size() const {
    return _end - _begin;
}

void Vector::clear() {
    Logger() << "> clear " << _id << "\n";
    for (size_t i = 0; i < size(); i++) {
        _begin[i].~value_type();
    }

    _end = _begin;
}

bool Vector::empty() const {
    return _end == _begin;
}

void Vector::erase(const_iterator pos) {
    const size_t offset = pos - begin();
    Logger() << "> erase " << _id << " " << offset << "\n";

    if (offset < size()) {
        for (size_t i = offset; i < size() - 1; i++) {
            _begin[i] = _begin[i + 1];
        }
        _begin[size() - 1].~value_type();
        _end--;
    }
}

void Vector::erase(const_iterator begin, const_iterator end) {
    const size_t offset_begin = begin - this->begin();
    const size_t offset_end   = end - this->begin();

    Logger() << "> eraserng " << _id << " " << offset_begin << " " << offset_end << "\n";

    const size_t count = offset_end - offset_begin;
    const size_t size_after = size() - count;

    for (size_t i = offset_begin; i < size_after; i++) {
        _begin[i] = _begin[i + count];
    }

    for (size_t i = size_after; i < size(); i++) {
        _begin[i].~value_type();
    }

    _end = _begin + size_after;
}

void Vector::insert(const_iterator pos, size_type count, const_reference val) {
    const size_t offset = pos - this->begin();

    Logger() << "> insert " << _id << " " << offset << " " << val << " " << count
             << "\n";

    // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    //           ^ offset
    // [0, 1, 2, ?, ?, ?, ?, 3, 4, 5, 6, 7, 8, 9]
    //                       ^ offset + count     ^ size + count

    if (capacity() < size() + count) {
        reserve(std::max(size() + count, size_type(2)));
    }

    for (size_t i = size() + count; i-- > size();) {
        new (_begin + i) value_type(_begin[i - count]);
    }

    for (size_t i = size(); i-- > offset + count;) {
        _begin[i] = _begin[i - count];
    }

    for (size_t i = offset; i < offset + count; i++) {
        _begin[i] = val;
    }

    _end += count;
}

void Vector::insert(
        const_iterator pos, const_iterator begin, const_iterator end) {
    const size_type offset = pos - this->begin();
    const size_type count = end - begin;

    Logger() << "> insertrng " << _id << " " << offset;

    if (capacity() < size() + count) {
        reserve(std::max(size() + count, size_type(2)));
    }

    // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    //           ^ offset
    // [0, 1, 2, ?, ?, ?, ?, 3, 4, 5, 6, 7, 8, 9]
    //                       ^ offset + count

    for (size_t i = size() + count; i-- > size();) {
        new (_begin + i) value_type(_begin[i - count]);
    }

    for (size_t i = size(); i-- > offset + count;) {
        _begin[i] = _begin[i - count];
    }

    for (size_t i = offset; i < offset + count; i++) {
        Logger() << " " << *begin;
        _begin[i] = *begin;
        begin++;
    }

    Logger() << "\n";

    _end += count;
}

void Vector::pop_back() {
    // deinit
    _begin[size() - 1].~value_type();
    _end--;
}

void Vector::pop_front() { erase(begin(), begin() + 1); }

void Vector::push_back(const_reference val) { insert(this->end(), 1, val); }

void Vector::push_front(const_reference val) { insert(this->begin(), 1, val); }

void Vector::reserve(size_type new_capacity) {
    // Logger() << "> res " << _id << " " << new_capacity << "\n";

    if (new_capacity <= capacity())
        return;

    pointer new_begin = static_cast<pointer>(operator new(sizeof(value_type) * new_capacity));
    for (size_t i = 0; i < size(); i++) {
        new (new_begin + i) value_type(_begin[i]);
        _begin[i].~value_type();
    }

    const size_t old_size = size();
    delete _begin;
    _begin = new_begin;
    _end = _begin + old_size;
    _end_cap = _begin + new_capacity;
}

void Vector::shrink_to_fit() {
    Logger() << "> shrink " << _id << " " << "\n";

    if (_end == _end_cap)
        return;

    pointer new_begin = static_cast<pointer>(operator new(sizeof(value_type) * size()));
    for (size_t i = 0; i < size(); i++) {
        new (new_begin + i) value_type(_begin[i]);
        _begin[i].~value_type();
    }

    const size_t old_size = size();
    delete _begin;
    _begin = new_begin;
    _end = _end_cap = _begin + old_size;
}

void Vector::dump() const {
    std::cout << _id << ": " << _begin << ", " << _end << ", " << _end_cap
              << "\n";
    std::cout << "len: " << size() << "\n";

    auto p = _begin;
    while (p != _end) {
        std::cout << *p << ", ";
        p++;
    }
    std::cout << "end\n";
}

} // namespace I2P2

#include "../header/I2P2_iterator.h"
#include <iostream>

namespace I2P2 {
iterator::iterator() : const_iterator() {}

iterator::iterator(iterator_impl_base* p) { this->_p = p->clone(); }

iterator::iterator(const iterator& rhs) { this->_p = rhs._p->clone(); }

iterator& iterator::operator++() {
    ++(*(this->_p));
    return *this;
}

iterator iterator::operator++(int) {
    iterator tmp{*this};
    ++(*(this->_p));
    return tmp;
}

iterator& iterator::operator--() {
    --(*(this->_p));
    return *this;
}

iterator iterator::operator--(int) {
    iterator tmp{*this};
    --(*(this->_p));
    return tmp;
}

iterator& iterator::operator+=(difference_type offset) {
    *(this->_p) += offset;
    return *this;
}

iterator iterator::operator+(difference_type offset) const {
    iterator tmp = *this;
    tmp._p->operator+=(offset);
    return tmp;
}

iterator& iterator::operator-=(difference_type offset) {
    *(this->_p) -= offset;
    return *this;
}

iterator iterator::operator-(difference_type offset) const {
    iterator tmp = *this;
    tmp._p->operator-=(offset);
    return tmp;
}

difference_type iterator::operator-(const iterator& rhs) const {
    return *(this->_p) - *(rhs._p);
}

pointer iterator::operator->() const { return this->_p->operator->(); }

reference iterator::operator*() const { return *(*(this->_p)); }

reference iterator::operator[](difference_type offset) const {
    return (*(this->_p))[offset];
}
} // namespace I2P2

#include "../header/I2P2_iterator.h"
#include <iostream>

namespace I2P2 {
list_iterator::list_iterator() { _node = nullptr; }

iterator_impl_base& list_iterator::operator++() {
    _node = _node->right;
    return *this;
}

iterator_impl_base& list_iterator::operator--() {
    _node = _node->left;
    return *this;
}

iterator_impl_base& list_iterator::operator+=(difference_type offset) {
    while (offset--) {
        _node = _node->right;
    }
    return *this;
}

iterator_impl_base& list_iterator::operator-=(difference_type offset) {
    while (offset--) {
        _node = _node->left;
    }
    return *this;
}

bool list_iterator::operator==(const iterator_impl_base& rhs) const {
    return this->_node == dynamic_cast<const list_iterator*>(&rhs)->_node;
}

bool list_iterator::operator!=(const iterator_impl_base& rhs) const {
    return this->_node != dynamic_cast<const list_iterator*>(&rhs)->_node;
}

bool list_iterator::operator<(const iterator_impl_base& rhs) const {
    Node* target = dynamic_cast<const list_iterator*>(&rhs)->_node;
    Node* cur    = _node->right;

    while (true) {
        if (cur == target)
            return true;
        if (cur->is_dummy)
            break;
        cur = cur->right;
    }

    return false;
}

bool list_iterator::operator>(const iterator_impl_base& rhs) const {
    Node* target = dynamic_cast<const list_iterator*>(&rhs)->_node;
    Node* cur    = _node->left;

    while (true) {
        if (cur == target)
            return true;
        if (cur->is_dummy)
            break;
        cur = cur->left;
    }

    return false;
}

bool list_iterator::operator<=(const iterator_impl_base& rhs) const {
    Node* target = dynamic_cast<const list_iterator*>(&rhs)->_node;
    Node* cur    = _node;

    while (true) {
        if (cur == target)
            return true;
        if (cur->is_dummy)
            break;
        cur = cur->right;
    }

    return false;
}

bool list_iterator::operator>=(const iterator_impl_base& rhs) const {
    Node* target = dynamic_cast<const list_iterator*>(&rhs)->_node;
    Node* cur    = _node;

    do {
        if (cur == target) {
            return true;
        }
        cur = cur->left;
    } while (!(cur->is_dummy));

    return false;
}

difference_type list_iterator::operator-(const iterator_impl_base& rhs) const {
    difference_type dist = 0;
    Node* target         = dynamic_cast<const list_iterator*>(&rhs)->_node;
    Node* cur            = _node;

    while (cur != target) {
        cur = cur->left;
        dist++;
    }

    return dist;
}

pointer list_iterator::operator->() const { return &(_node->data); }

reference list_iterator::operator*() const {
    return _node->data;
}

reference list_iterator::operator[](difference_type offset) const {
    Node* cur = _node;

    if (offset >= 0) {
        for (difference_type i = 0; i < offset; i++) {
            cur = cur->right;
        }
    } else {
        for (difference_type i = 0; i > offset; i--) {
            cur = cur->left;
        }
    }

    return cur->data;
}
} // namespace I2P2
